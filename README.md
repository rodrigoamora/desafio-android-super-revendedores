# Criar um aplicativo de consulta na api do [Dribbble](https://dribbble.com)#

Criar um aplicativo para consultar a [Dribbble API](http://developer.dribbble.com/v1/) e trazer os shots + populares . Basear-se no mockup fornecido:

![Screen mockup-dribble-desafio.png](https://bytebucket.org/adminsuper/desafio-android-super-revendedores/raw/5832d83a2defd92faec71c69576e0f2631916194/image/mockup-dribble-desafio.png)

### **Deve conter** ###

* Lista de shots . API (http://api.dribbble.com/shots/popular?page=1)
* Paginação na tela de listas.
* Detalhe de um shot . API(http://api.dribbble.com/shots/1757954)
* Tela de detalhe de um shot deve conter Autor com foto.
* Lazy Load de Imagens

### Adicionais ###

* Gestão de dependencias no projeto. Ex: [Gradle]
* Mapeamento json -> Objeto 
* Framework para Comunicação com API 
* Testes unitários no projeto 
* Testes funcionais
* App Universal
* Cache de Imagens
* Cache dos dados
 

### **Sugestões** ###

Pode-se utilizar das libs que preferir. Há muitos lugares de referência como [Android Arsenal](https://android-arsenal.com/)

Caso tenha alguma dúvida, o [CodePath](https://guides.codepath.com/android) é um bom lugar para dar uma olhada. Não impedimos 
que outros lugares sejam consultados.

Use de preferencia **JAVA** para o desafio, mas saber Kotlin é um diferencial.

Bons usos do material designer são apreciados pelo nosso designer! :D

Coloque o link do github das libs que usar no Read.me ou na Doc do PR que fizer, para que possamos analizar.

### **OBS** ###

A foto do mockup é meramente ilustrativa, não existe necessidade de fazer share em redes sociais. Consideraremos como um plus caso seja feito.  


### **Processo de submissão** ###
O candidato deverá implementar a solução e enviar um PR (pull request) para este repositório com a solução.

Boa Sorte!