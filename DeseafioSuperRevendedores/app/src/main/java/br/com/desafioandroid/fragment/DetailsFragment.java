package br.com.desafioandroid.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.desafioandroid.R;

public class DetailsFragment extends Fragment {

    private TextView title, description, views, comments, created;
    private ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container,	false);

        title = (TextView) rootView.findViewById(R.id.title_shot);
        title.setText(getArguments().getString("nameShot"));

        description = (TextView) rootView.findViewById(R.id.description);
        description.setText(getArguments().getString("description"));

        views = (TextView) rootView.findViewById(R.id.views_count);
        views.setText(getArguments().getString("views"));

        created = (TextView) rootView.findViewById(R.id.created_at);
        created.setText(getArguments().getString("created"));

        comments = (TextView) rootView.findViewById(R.id.comments_counts);
        comments.setText(getArguments().getString("comments"));

        image = (ImageView) rootView.findViewById(R.id.image);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Picasso.with(getActivity()).load(getArguments().getString("image")).into(image);
    }

}
