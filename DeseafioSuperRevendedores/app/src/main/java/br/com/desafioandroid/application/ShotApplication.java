package br.com.desafioandroid.application;

import android.app.Application;

import br.com.desafioandroid.component.DaggerShotComponent;
import br.com.desafioandroid.component.ShotComponent;

public class ShotApplication extends Application {

    private ShotComponent component;

    @Override
    public void onCreate() {
        component = DaggerShotComponent.builder().build();
    }

    public ShotComponent getComponent() {
        return component;
    }

}
