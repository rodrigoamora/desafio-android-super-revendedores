package br.com.desafioandroid.module;

import br.com.desafioandroid.BuildConfig;
import br.com.desafioandroid.callback.ShotsCallback;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ShotModule {

    @Provides
    public ShotsCallback getShotsCallBack() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ShotsCallback service = retrofit.create(ShotsCallback.class);
        return service;
    }

}
