package br.com.desafioandroid.component;

import br.com.desafioandroid.activity.MainActivity;
import br.com.desafioandroid.fragment.ShotFragment;
import br.com.desafioandroid.module.ShotModule;
import dagger.Component;

@Component(modules=ShotModule.class)
public interface ShotComponent {

    void inject(MainActivity activity);
    void inject(ShotFragment fragment);

}
