package br.com.desafioandroid.callback;

import java.util.List;

import br.com.desafioandroid.shared.model.Shot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShotsCallback {

    @GET("shots/")
    public Call<List<Shot>> listShots(@Query("access_token")String accessToken, @Query("page")int page, @Query("per_page")int perPage);

}
