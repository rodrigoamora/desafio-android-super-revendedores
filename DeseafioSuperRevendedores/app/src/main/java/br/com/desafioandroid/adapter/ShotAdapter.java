package br.com.desafioandroid.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioandroid.util.ChangeFragment;
import br.com.desafioandroid.R;
import br.com.desafioandroid.fragment.DetailsFragment;
import br.com.desafioandroid.shared.model.Shot;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotAdapter extends RecyclerView.Adapter<ShotAdapter.ShotViewHolder> {

    private Activity context;
    private List<Shot> shots;

    public OnLoadMoreListener loadMoreListener;

    public ShotAdapter(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public ShotAdapter(Activity context, List<Shot> shots) {
        this.context = context;
        this.shots = shots;
    }

    @Override
    public ShotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_shot, parent, false);
        ShotViewHolder holder = new ShotViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShotViewHolder holder, final int position) {
        Picasso.with(context).load(shots.get(position).getImages().getNormal()).placeholder(R.mipmap.ic_launcher).into(holder.image);

        holder.nameShot.setText(shots.get(position).getTitle());
        holder.nameShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeFragment.changeFragment(R.id.conatiner, DetailsFragment.class, context.getFragmentManager(), false, getBundle(shots.get(position)));
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeFragment.changeFragment(R.id.conatiner, DetailsFragment.class, context.getFragmentManager(), false, getBundle(shots.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return shots.size();
    }

    private Bundle getBundle(Shot shot) {
        Bundle bundle = new Bundle();
        bundle.putString("nameShot", shot.getTitle());
        bundle.putString("description", shot.getDescription());
        bundle.putString("views", shot.getViews_count());
        bundle.putString("created", shot.getCreated_at());
        bundle.putString("comments", shot.getTitle());
        bundle.putString("image", shot.getImages().getNormal());
        return bundle;
    }

    public class ShotViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_shot)
        TextView nameShot;

        @BindView(R.id.image)
        ImageView image;

        public ShotViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface OnLoadMoreListener {
        public void onLoadMore(int page, int totalItemsCount);
    }

}
