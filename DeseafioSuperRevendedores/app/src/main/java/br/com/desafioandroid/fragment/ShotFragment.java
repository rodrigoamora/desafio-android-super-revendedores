package br.com.desafioandroid.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.desafioandroid.BuildConfig;
import br.com.desafioandroid.R;
import br.com.desafioandroid.adapter.ShotAdapter;
import br.com.desafioandroid.application.ShotApplication;
import br.com.desafioandroid.callback.ShotsCallback;
import br.com.desafioandroid.component.ShotComponent;
import br.com.desafioandroid.listener.PaginateListener;
import br.com.desafioandroid.shared.Util;
import br.com.desafioandroid.shared.model.Shot;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShotFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    ShotsCallback service;

    private List<Shot> shots;
    private ShotAdapter shotAdapter;
    private int page;

    public ShotFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,	false);
        ButterKnife.bind(this, rootView);

        shots = new ArrayList<Shot>();
        page = 1;

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getComponents();
        configureRecyclerView();
        listShots();
    }

    private void configureRecyclerView() {
        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setOnScrollListener(new PaginateListener(linearLayout) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                listShots();
            }
        });
    }

    private void getComponents() {
        ShotApplication app = (ShotApplication) getActivity().getApplication();
        ShotComponent component = app.getComponent();
        component.inject(this);
    }

    private void listShots() {
        if (!Util.checkConnection(getActivity())) {
            Util.showToast(getActivity(), getString(R.string.no_internet));
            return;
        } else {
            Util.showProgressDiaolg(getActivity(), getString(R.string.searching_shots), getString(R.string.wait), false);
            Call<List<Shot>> call = service.listShots(BuildConfig.ACCESS_TOKEN, page, 30);
            call.enqueue(new Callback<List<Shot>>() {
                @Override
                public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
                    if (response.body().isEmpty()) {
                        Util.showToast(getActivity(), getString(R.string.no_shots));
                    } else {
                        shots.addAll(response.body());
                        populateRecyclerView();
                        if (page == 1) {
                            populateRecyclerView();
                        } else {
                            atualizarRecyclerView();
                        }
                    }
                    Util.dimissProgressDialog();
                }

                @Override
                public void onFailure(Call<List<Shot>> call, Throwable t) {
                    Util.showToast(getActivity(), getString(R.string.error_get_shots));
                    Util.dimissProgressDialog();
                }
            });
        }
    }

    private void populateRecyclerView() {
        shotAdapter = new ShotAdapter(getActivity(), shots);
        recyclerView.setAdapter(shotAdapter);
    }

    private void atualizarRecyclerView() {
        shotAdapter.notifyDataSetChanged();
    }

    public List<Shot> getShots() {
        return shots;
    }

}
