package br.com.desafioandroid.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import br.com.desafioandroid.R;
import br.com.desafioandroid.fragment.ShotFragment;
import br.com.desafioandroid.util.ChangeFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar();

        ChangeFragment.changeFragment(R.id.conatiner, ShotFragment.class, getFragmentManager(), false, null);
    }

    private void actionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

}
